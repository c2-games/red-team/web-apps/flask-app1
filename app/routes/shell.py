from flask import request, Blueprint, render_template, jsonify
from datetime import datetime
import subprocess

shell_bp = Blueprint("shell", __name__)


@shell_bp.route("/shell", methods=["GET"])
def shell():
    return render_template("shell.html")


@shell_bp.route("/execute", methods=["POST"])
def execute_command():
    data = request.get_json()
    command = data["command"]
    time_executed = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    try:
        output = subprocess.check_output(
            command, shell=True, text=True, stderr=subprocess.STDOUT
        )
    except subprocess.CalledProcessError as e:
        output = e.output
    return jsonify({"output": output, "command": command, "time": time_executed})
