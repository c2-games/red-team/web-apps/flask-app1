import logging
from flask import Blueprint, render_template, redirect, request, url_for

from app.database import get_db_connection
from .session import get_user

forgot_password_bp = Blueprint("forgot-password", __name__)


@forgot_password_bp.route("/forgot-password", methods=["GET"])
@forgot_password_bp.route("/forgot-password.html", methods=["GET"])
def forgot_password() -> str:
    user = get_user()

    if user is None:
        return render_template("forgot-password.html")
    return redirect(url_for("index.index"))


@forgot_password_bp.route("/forgot-password", methods=["POST"])
@forgot_password_bp.route("/forgot-password.html", methods=["POST"])
def post() -> str:
    username = request.form.get("username")
    password = request.form.get("password")

    if not username or not password:
        return render_template(
            "forgot-password.html", error="Missing username or password"
        )

    try:
        with get_db_connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    f"""
                    UPDATE users SET password = '{password}' WHERE username = '{username}'
                    """
                )
                cur.close()

                return redirect(url_for("index.index"))
    except Exception as e:
        logging.error(e)

        return render_template("forgot-password.html", error=e)
