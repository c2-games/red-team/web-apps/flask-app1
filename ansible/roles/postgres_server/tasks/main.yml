---
- name: Update and refresh repository cache
  apt:
    update_cache: yes
    cache_valid_time: 3600
  become: true

- name: Install PostgreSQL packages
  apt:
    name: "{{ item }}"
    state: present
  become: true
  loop:
    - postgresql
    - postgresql-contrib
    - libpq-dev
    - python3
    - python3-pip

- name: Install psycopg2 using pip
  become: true
  pip:
    name: psycopg2
    executable: pip3

- name: Ensure PostgreSQL service is running
  service:
    name: postgresql
    state: started
    enabled: yes
  become: true

- name: Configure PostgreSQL to listen on all interfaces
  become: true
  lineinfile:
    path: /etc/postgresql/12/main/postgresql.conf
    regexp: '^#?listen_addresses'
    line: "listen_addresses = '*'"
    state: present

- name: Set PostgreSQL password for the PostgreSQL user
  postgresql_user:
    name: "{{ postgres_user }}"
    password: "{{ postgres_password }}"
  become: true
  become_user: postgres

- name: Allow PostgreSQL port in UFW
  become: true
  ufw:
    rule: allow
    port: '5432'
    proto: tcp

- name: Allow all IPs to connect to PostgreSQL
  lineinfile:
    path: /etc/postgresql/12/main/pg_hba.conf
    line: "host    all             all             0.0.0.0/0               md5"
    state: present

- name: Create a PostgreSQL database
  become_user: postgres
  postgresql_db:
    name: "{{ postgres_db }}"
    encoding: UTF8
    lc_collate: en_US.UTF-8
    lc_ctype: en_US.UTF-8
    template: template0
    state: present

- name: Change PostgreSQL port
  lineinfile:
    path: /etc/postgresql/12/main/postgresql.conf
    regexp: '^#?port ='
    line: "port = {{ postgres_port }}"
    state: present
  notify:
    - restart postgresql
